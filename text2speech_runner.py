import bentoml
import torch
from __init__path import *
from third_party.vits.text import text_to_sequence
import third_party.vits.commons as commons
import third_party.vits.utils as utils
from third_party.vits.models import SynthesizerTrn
from torch import no_grad, LongTensor
import numpy as np


class Text2SpeechRunnable(bentoml.Runnable):
    SUPPORTED_RESOURCES = ("nvidia.com/gpu", "cpu")
    SUPPORTS_CPU_MULTI_THREADING = True

    def __init__(self):
        self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
        self.language_marks = {
            "Japanese": "",
            "日本語": "[JA]",
            "中文": "[ZH]",
            "English": "[EN]",
            "Mix": "",
            }
        self.lang = ['日本語', '中文', 'English', 'Mix']
        self.hps = utils.get_hparams_from_file("./third_party/vits/finetune_speaker.json")
        self.set_model()

    def set_model(self):
        self.net_g = SynthesizerTrn(
            len(self.hps.symbols),
            self.hps.data.filter_length // 2 + 1,
            self.hps.train.segment_size // self.hps.data.hop_length,
            n_speakers=self.hps.data.n_speakers,
            **self.hps.model).to(self.device)
        _ = self.net_g.eval()

        _ = utils.load_checkpoint("./third_party/vits/OUTPUT_MODEL/G_500.pth", self.net_g, None)
        self.speaker_ids = self.hps.speakers

    def get_text(self,text, hps, is_symbol):
        text_norm = text_to_sequence(text, hps.symbols, [] if is_symbol else hps.data.text_cleaners)
        if hps.data.add_blank:
            text_norm = commons.intersperse(text_norm, 0)
        text_norm = LongTensor(text_norm)
        return text_norm

    def create_tts_fn(self):
        def tts_fn(text, speaker, language, speed):
            if language is not None:
                text = self.language_marks[language] + text + self.language_marks[language]
            speaker_id = self.speaker_ids[speaker]
            stn_tst = self.get_text(text, self.hps, False)
            with no_grad():
                x_tst = stn_tst.unsqueeze(0).to(self.device)
                x_tst_lengths = LongTensor([stn_tst.size(0)]).to(self.device)
                sid = LongTensor([speaker_id]).to(self.device)
                audio = self.net_g.infer(x_tst, x_tst_lengths, sid=sid, noise_scale=.667, noise_scale_w=0.8,
                                    length_scale=1.0 / speed)[0][0, 0].data.cpu().float().numpy()
            del stn_tst, x_tst, x_tst_lengths, sid
            return "Success", (self.hps.data.sampling_rate, audio)

        return tts_fn

    @bentoml.Runnable.method(batchable=False)
    def generate_speech(self,text, speaker, language, speed):
        if language is not None:
            text = self.language_marks[language] + text + self.language_marks[language]
        speaker_id = self.speaker_ids[speaker]
        stn_tst = self.get_text(text, self.hps, False)
        with no_grad():
            x_tst = stn_tst.unsqueeze(0).to(self.device)
            x_tst_lengths = LongTensor([stn_tst.size(0)]).to(self.device)
            sid = LongTensor([speaker_id]).to(self.device)
            audio = self.net_g.infer(x_tst, x_tst_lengths, sid=sid, noise_scale=.667, noise_scale_w=0.8,
                                length_scale=1.0 / speed)[0][0, 0].data.cpu().float().numpy()
        del stn_tst, x_tst, x_tst_lengths, sid
        return "Success", (self.hps.data.sampling_rate, audio)